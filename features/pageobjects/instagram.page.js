class InstagramPage {

    get tabSearch () {
        return $('id:search_tab')
    }

    get inputSearch () {
        return $('id:action_bar_search_edit_text')
    }

    getRowSearchByIndex (index) {
        return $$('id:row_search_user_username')[index]
    }

    get follow (){
        return $('id:profile_header_user_action_follow_button')
    }

    async clickTabSeacrh () {
        await this.tabSearch.click()
    }
    
    async fillInputSearch (username) {
        await this.inputSearch.click()
        await this.inputSearch.setValue(username)
    }

    async clickRowSearch (index) {
        await this.getRowSearchByIndex(index).click()
        await this.follow.click()
    }

}

export default new InstagramPage()
