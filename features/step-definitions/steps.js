import { Given, When, Then } from '@wdio/cucumber-framework'
import InstagramPage from '../pageobjects/instagram.page.js'

Given('I am click tab search', async ()=> {
    await InstagramPage.clickTabSeacrh()
});

When('I am fill field search with {string}', async (username) => {
    await InstagramPage.fillInputSearch(username)
});

Then('I follow flip.id on the {int} row', async (index) => {
    await InstagramPage.clickRowSearch(index - 1)
});
